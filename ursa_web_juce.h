/*******************************************************************************
 The block below describes the properties of this module, and is read by
 the Projucer to automatically generate project code that uses it.
 For details about the syntax and how to create or use a module, see the
 JUCE Module Format.txt file.


 BEGIN_JUCE_MODULE_DECLARATION

  ID:               ursa_web_juce
  vendor:           juce
  version:          5.4.1
  name:             Ursa Web Juce
  description:      Utility classes for Juce apps web connections
  
  website:          http://www.juce.com/juce
  license:          Unrestricted

  dependencies:
  OSXFrameworks:    
  iOSFrameworks:    
  linuxLibs:        
  mingwLibs:        

 END_JUCE_MODULE_DECLARATION

*******************************************************************************/
#pragma once

#include <juce_core/juce_core.h>
#include <juce_gui_basics/juce_gui_basics.h>
#include <juce_cryptography/juce_cryptography.h>

#include <array>
#include "json.hpp"
#include <vector>

namespace ursa
{
	#include "urls/ursa_web_utils.h"

}
