/*
  ==============================================================================

    ursa_web_utils.cpp
    Created: 29 Mar 2019 9:20:20am
    Author:  davee

  ==============================================================================
*/

#include "ursa_web_utils.h"
#include <utility>

using namespace nlohmann;


UrsaUrlMultiPartForm::UrsaUrlMultiPartForm (): boundary (String::toHexString (Random::getSystemRandom ().nextInt64 ()))
{
}

void UrsaUrlMultiPartForm::addFile (const File& file, const String& parameterName, const String& fileMimeType,
    const String& fileName)
{
    /*const FileParam fp{parameterName, fileName, fileMimeType, file};
    fileParams.push_back (fp);*/
    fileParams.emplace_back (parameterName, fileName, fileMimeType, file);
}

void UrsaUrlMultiPartForm::addFile (const File& file, const String& parameterName, const String& fileMimeType)
{
    addFile (file, parameterName, fileMimeType, file.getFileName ());
}

void UrsaUrlMultiPartForm::addParam (const String& key, const String& value)
{
    /*const std::pair<String, String> pair = { key,value };
    textParams.push_back (pair);*/
    textParams.emplace_back (key, value);
}

String UrsaUrlMultiPartForm::getContentTypeHeader () const
{
    return "Content-Type: multipart/form-data; boundary=" + boundary + "\r\n";
}

MemoryBlock UrsaUrlMultiPartForm::encodeData () const 
{
    MemoryBlock request_entity_body;
    MemoryOutputStream data (request_entity_body, false);

    data << "--" << boundary;

    // first write out the text parameters
    for (const auto& pair : textParams)
    {
        data << "\r\nContent-Disposition: form-data; name=\"" << pair.first
            << "\"\r\n\r\n" << pair.second
            << "\r\n--" << boundary;
    }

    // next the files
    for(const auto& f: fileParams)
    {
        data << "\r\nContent-Disposition: form-data; name=\"" << f.paramName
            << "\"; filename=\"" << f.filename << "\"\r\n";

        if (f.mimeType.isNotEmpty ())
            data << "Content-Type: " << f.mimeType << "\r\n";

        data << "Content-Transfer-Encoding: binary\r\n\r\n";
        data << f.file;
        data << "\r\n--" << boundary;
    }
    data << "--\r\n";

    data.flush ();

    return request_entity_body;
}

UrsaUrl::UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
    const MemoryBlock& request_entity_body, HttpMethod method)
    : baseURL (url),
    url_parameters (url_parameters),
    extra_headers (extra_headers),
    request_entity_body_block (request_entity_body),
    method (method)
{
    int contentTypeFound = 0;
    for (const auto& header : extra_headers)
    {
        if (header.startsWithIgnoreCase ("Content-Type:"))
            contentTypeFound++;
    }

    // one and only one content type required
    jassert (contentTypeFound == 1);

}

UrsaUrl::UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
                  const UrsaUrlMultiPartForm& request_entity_body, HttpMethod method): UrsaUrl (
    url, url_parameters, addContentTypeIfMissing (extra_headers, request_entity_body.getContentTypeHeader ()),
    request_entity_body.encodeData (), method)
{
}

UrsaUrl::UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
                  const String& request_entity_body, HttpMethod method)
    : UrsaUrl (
        url, url_parameters, addContentTypeIfMissing (extra_headers, "Content-Type: text/plain"),
        MemoryBlock (request_entity_body.toRawUTF8 (), request_entity_body.getNumBytesAsUTF8 ()), method)

{
}

UrsaUrl::UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
                  const char* request_entity_body, HttpMethod method): UrsaUrl (
    url, url_parameters, addContentTypeIfMissing (extra_headers, "Content-Type: text/plain"),
    String (request_entity_body), method)
{
}

UrsaUrl::UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
                  const StringPairArray& body_params_to_encode, HttpMethod method) : UrsaUrl (
    url, url_parameters, addContentTypeIfMissing (extra_headers, "Content-Type: application/x-www-form-urlencoded"),
    urlEncodeFormParams (body_params_to_encode), method)
{
}

UrsaUrl::UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
                  const nlohmann::json& request_entity_body_json, HttpMethod method):
    UrsaUrl (url, url_parameters, addContentTypeIfMissing (extra_headers, "Content-Type: application/json"),
             String (request_entity_body_json.dump (2)), method)
{
}

UrsaUrl UrsaUrl::createFromFile(const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
    const File& fileToSendAsBody, const String& contentTypeHeader, HttpMethod method)
{
    MemoryBlock block;

	if (!fileToSendAsBody.loadFileAsData(block))
	{
		//Logger::writeToLog("Could not load file as data: " + fileToSendAsBody.getFullPathName());
        jassertfalse;
	}

	return UrsaUrl(url, url_parameters, addContentTypeIfMissing(extra_headers, contentTypeHeader),
		block, method);
}


UrsaUrl::~UrsaUrl()
{
    if (!logWritten && callResults != nullptr)
        read_output_and_write_to_log();
}


int UrsaUrl::call (int minSuccess, int maxSuccess, int timeout)
{
    if (callResults != nullptr)
    {
        // An instance of this class is single use only
        jassertfalse;
        return -1;
    }

    callResults = std::make_unique<UrlCallResults> ();

    if(url_parameters.size()>0)
    {
        const URL baseUrlAsURL (baseURL);

        // if you hit the assert below, your baseURL had url_parameters, and you've provided MORE url parameters, 
        // not sure that will work.
        jassert (baseURL.equalsIgnoreCase (baseUrlAsURL.toString (false)));
    }

    // the following lines sidestep JUCE URL's odd handling of parameters, and allow the user to add them separately

    URL url2 = URL(baseURL).withParameters (url_parameters);
    const auto strUrlWithParams = url2.toString (true);
    callResults->urlWithParams = URL::createWithoutParsing (strUrlWithParams);

    if (request_entity_body_block.getSize ()>0)
    {
        //there's a body, we probably need to warn if it's being attached to a method that doesn't support a body
        jassert(method == HttpMethod::POST || method == HttpMethod::PUT);
        callResults->urlWithParams = callResults->urlWithParams.withPOSTData (request_entity_body_block);
    }

    String request_message_headers;

    for (const auto& header : extra_headers)
    {
        if (!header.endsWith ("\r\n"))
            request_message_headers += header + "\r\n";
        else
            request_message_headers += header;
    }

    URL& jurl = callResults->urlWithParams;

    const auto startTime = Time::currentTimeMillis ();

    callResults->input_stream = jurl.createInputStream (true, nullptr, nullptr, request_message_headers, timeout,
        &callResults->response_headers, &callResults->status_code, 5, http_method_to_string (method));
    
    callResults->callDurationMillis = Time::currentTimeMillis () - startTime;

    if (callResults->status_code < minSuccess || callResults->status_code > maxSuccess)
    {
        innerWriteToLog (get_raw_string_response ());
        jassertfalse;
    }

    return callResults->status_code;
}

void recursiveRedactItems(json& j, const StringArray& logBlacklist)
{
	try
	{
		for (auto& element : j.items())
		{
            String key = element.key();
			
            if (logBlacklist.contains(key))
            {
                element.value() = "*redacted*";
                continue;
            }
            auto& value = element.value();
			
			if (value.is_structured())
                recursiveRedactItems(value, logBlacklist);
            
		}
	}
	catch (...)
	{
		DBG("Exception");
	}
}

String UrsaUrl::getResponseContentType() const
{
    if (callResults == nullptr)
    {
        jassertfalse;
        innerWriteToLog ("Not Yet Called");
        return{};
    }

    if (callResults->callDurationMillis < 0)
    {
        jassertfalse;
        innerWriteToLog("Call not completed");
        return{};
    }

    return callResults->response_headers.getValue ("Content-Type", "");
}

nlohmann::json UrsaUrl::get_json_response(StringArray logBlacklist, bool logging) const
{
    if (callResults == nullptr)
    {
        jassertfalse;
        innerWriteToLog ("Not Yet Called");
        return{};
    }

    if (callResults->callDurationMillis < 0)
    {
        jassertfalse;
        innerWriteToLog ("Call not completed");
        return{};
    }

    auto content_type= callResults->response_headers.getValue ("Content-Type", "");

    if(!content_type.containsIgnoreCase ("application/json"))
    {
        innerWriteToLog ("Content Type is ["+ content_type+"]");
        //content type doesn't suggest JSON response
        jassertfalse;
        
        return {};
    }
    const auto str = callResults->input_stream->readEntireStreamAsString ();
    json rval;

    if (str.isEmpty())
    {
    	//this is not valid JSON, is it possible you called get_json_response already or are getting the response from the wrong url?
        jassertfalse;
        return{};
    }
	
    try {
        rval = json::parse (str.toRawUTF8 ());

        if (logging)
        {
            if (logBlacklist.isEmpty())
            {
                innerWriteToLog(rval.dump(2));
            }
            else
            {
                auto logJson = rval;
                recursiveRedactItems(logJson, logBlacklist);
                innerWriteToLog(logJson.dump(2));
            }
        }
        else
        {
            // this stops a last-ditch logging in the destructor of this class
            logWritten = true;
        }
    }
    catch(const nlohmann::detail::parse_error& /*e*/)
    {
        Logger::writeToLog ("Could not parse 'json' response: ");
        innerWriteToLog (str);
    }

    return rval;
}

String UrsaUrl::getStringResponse () const
{
    String str;

    if(callResults==nullptr)
    {
        jassertfalse;
        innerWriteToLog ("Not Yet Called");
        return{};
    }

    if(callResults->input_stream==nullptr)
    {
        if (callResults->callDurationMillis < 0) 
        {
            jassertfalse;
            innerWriteToLog ("Call not completed");
        }
        else
        {
            innerWriteToLog ("Null response");
        }
    }
    else
    {
        str = callResults->input_stream->readEntireStreamAsString ();

        innerWriteToLog (str);

    }
    return str;
}

String UrsaUrl::findHeaderStarting(const String& start) const
{
	for(const auto& header: extra_headers)
	{
		if(header.startsWithIgnoreCase(start))
			return header.substring(start.length());
	}
	return {};
}

String UrsaUrl::get_raw_string_response () const
{
    if(callResults->input_stream==nullptr)
    {
        return "Null Response";
    }
    auto str = callResults->input_stream->readEntireStreamAsString ();
    return str;
}

bool UrsaUrl::getAndStoreFileResponse (const File& saveFile) const
{
    if (callResults == nullptr)
    {
        // call first then get results...
        jassertfalse;
        return false;
    }

    if (callResults->input_stream == nullptr)
    {
        return false;
    }

    FileOutputStream fos{ saveFile };

    while (!callResults->input_stream->isExhausted ()) {
        fos.writeFromInputStream (*callResults->input_stream, 8192);
        if (Thread::currentThreadShouldExit ())
            return false;
    }

    return true;
}

Image UrsaUrl::getImageResponse () const
{
    if (callResults->input_stream == nullptr)
        return {};
    const auto bis = std::make_unique<BufferedInputStream>( callResults->input_stream.get(), 4096, false );
    ImageFileFormat* format = ImageFileFormat::findImageFormatForStream (*bis);

    if(format==nullptr)
	{
		jassertfalse;
		return {};
	}

    return format->decodeImage (*bis);
}

void UrsaUrl::read_output_and_write_to_log () const
{
    innerWriteToLog (get_raw_string_response());
}

void UrsaUrl::sensitiveLogging ()
{
    authenticationCall = true;
}

void UrsaUrl::logTimeMetrics ()
{
	if (callResults != nullptr) {
		Logger::writeToLog("URL                 :  " + callResults->urlWithParams.toString(true));
		Logger::writeToLog("Call Duration       :  " + String(callResults->callDurationMillis) + "ms");
	}
}

String UrsaUrl::http_method_to_string (HttpMethod method)
{
    switch (method)
    {
    case HttpMethod::GET: return "GET";
    case HttpMethod::HEAD: return "HEAD";
    case HttpMethod::POST: return "POST";
    case HttpMethod::PUT: return "PUT";
    case HttpMethod::DELETE: return "DELETE";
    case HttpMethod::CONNECT: return "CONNECT";
    case HttpMethod::OPTIONS: return "OPTIONS";
    case HttpMethod::TRACE: return "TRACE";
    case HttpMethod::PATCH: return "PATCH";
    default:
        jassertfalse;
        return "UNKNOWN";
    }
}


/*
void UrsaUrl::innerWriteLogShortened (const String& responseText) const
{
    if (callResults != nullptr) {
        Logger::writeToLog ("URL                 :  " + callResults->urlWithParams.toString (true));
        Logger::writeToLog ("Call Duration       :  " + String (callResults->callDurationMillis) + "ms");
        Logger::writeToLog ("Status code         :  " + String (callResults->status_code));
    }
    else
    {
        Logger::writeToLog ("URL (not yet called):  " + baseURL);
    }

    if (authenticationCall)
    {
        Logger::writeToLog ("Request entity body :  suppressed for security");
    }
    else if (request_entity_body_block.getSize () < 640)
    {
        Logger::writeToLog ("Request entity body :  " + request_entity_body_block.toString ());
    }
    else
    {
        MemoryBlock headCopy;
        headCopy.copyFrom (request_entity_body_block.getData (), 0, 300);

		MemoryBlock tailCopy;
		tailCopy.copyFrom(request_entity_body_block.getData()+ request_entity_body_block.getSize() - 320, 300 , 320);

        Logger::writeToLog ("Request entity body :  " + headCopy.toString () + "\n... (truncated)\n" + tailCopy.toString ());
    }

    if (callResults != nullptr)
    {
        auto keys = callResults->response_headers.getAllKeys ();
        String header_output;
        for (const auto& key : keys)
        {
            header_output += key + " " + callResults->response_headers[key] + "  ";
        }
        Logger::writeToLog ("Response headers    : " + header_output);

		if (authenticationCall)
		{
			Logger::writeToLog("Response Body       :  suppressed for security");
		}
		else if (responseText.length() > 640)
		{

			Logger::writeToLog("Response Body (cut) :  " + responseText.substring(0, 300) 
				+ "\n... (truncated)\n"
			+ responseText.substring (responseText.length ()-320));
		}
		else
		{
			Logger::writeToLog("Response Body       :  " + responseText);
		}
    }
    else if (!responseText.isEmpty ())
    {
        Logger::writeToLog ("Info                :  " + responseText);
    }
}
*/

void UrsaUrl::innerWriteToLog (String responseText, bool forceVerbose) const
{
    jassert(logWritten != true);
    logWritten = true;
    if (callResults != nullptr) {
        Logger::writeToLog ("URL                 :  " + callResults->urlWithParams.toString (true));
        Logger::writeToLog ("METHOD              :  " + http_method_to_string(method));
        Logger::writeToLog ("Call Duration       :  " + String (callResults->callDurationMillis) + "ms");
        Logger::writeToLog ("Status code         :  " + String (callResults->status_code));
        Logger::writeToLog ("Token               : " + findHeaderStarting("token:"));
    	Logger::writeToLog ("Content-Type        : " + findHeaderStarting("Content-Type:"));

		if(callResults->status_code>=200&& callResults->status_code<300)
			forceVerbose = true;
		

#ifdef  DEBUG
		forceVerbose = true;
#endif

    }
    else
    {
        Logger::writeToLog ("URL (not yet called):  " + baseURL);
    }

	if (authenticationCall)
	{
		Logger::writeToLog("Request entity body :  suppressed for security");
	}
	else if (request_entity_body_block.getSize() < 512)
	{
		Logger::writeToLog("Request entity body :  " + request_entity_body_block.toString());
	}
	else
	{
        MemoryBlock headCopy(512, true);
		headCopy.copyFrom(request_entity_body_block.getData(), 0, 512);

		Logger::writeToLog("Request entity body :  " + headCopy.toString() + "\n...truncated");
	}


    if (callResults != nullptr) {
        auto keys = callResults->response_headers.getAllKeys ();
        String header_output;
        for (const auto& key : keys)
        {
            header_output += key + " " + callResults->response_headers[key] + "  ";
        }
        Logger::writeToLog (   "Response headers    : "+ header_output);

		if (authenticationCall)
		{
			Logger::writeToLog("Response Body       :  suppressed for security");
		}
		else if (responseText.length() > 500 && !forceVerbose)
		{
			Logger::writeToLog("Response Body       :  " + responseText.substring(0, 200)
				+ "\n                 ...truncated...\n"
				+ responseText.substring(responseText.length() - 300)+"\n");
		}
		else
		{
			Logger::writeToLog("Response Body       :  " + responseText);
		}
    }
    else if (!responseText.isEmpty())
    {
        Logger::writeToLog ("Info                :  " + responseText);
    }

    Logger::writeToLog ("\r\n");

}

String UrsaUrl::urlEncodeFormParams (const StringPairArray& stringPairs)
{
    String output;
    for(int x=0; x< stringPairs.size (); x++)
    {
        const auto key = stringPairs.getAllKeys ()[x];
        const auto value = stringPairs.getAllValues ()[x];

        output += (output.isEmpty () ? "" : "&") +
            URL::addEscapeChars (key, false, false) +
            "=" +
            URL::addEscapeChars (value, true, false);
    }

    return output;
}


MemoryBlock hmacSha256(MemoryBlock key, MemoryBlock message)
{
    //see: https://en.wikipedia.org/w/index.php?title=HMAC&oldid=943555033

    constexpr int blockSize = 64;
    constexpr int outputSize = 32;
    constexpr uint8 iPadFill = 0x36;
    constexpr uint8 oPadFill = 0x5c;

    // if it is longer than blocksize we hash Key first (NOTE: the output of hash may be < blocksize)
    if (key.getSize() > blockSize)
    {
        const SHA256 keySha(key);
        key = keySha.getRawData();
        jassert(key.getSize() == outputSize);
    }

    // if key is less than block size, we right pad with 0s 
    key.ensureSize(blockSize, true);

    //if key was exactly block size to start with, that's fine, we leave it as is.

    // create inner and outer padded keys
    MemoryBlock iKeyPad(blockSize);
    MemoryBlock oKeyPad(blockSize);
    iKeyPad.fillWith(iPadFill);
    oKeyPad.fillWith(oPadFill);

    for (size_t i = 0; i < iKeyPad.getSize(); ++i)
    {
        iKeyPad[i] ^= key[i];
        oKeyPad[i] ^= key[i];
    }

    // i_key_pad || message
    iKeyPad.append(message.getData(), message.getSize());

    // hash (i_key_pad || message)
    const SHA256 innerHash(iKeyPad);
    auto innerHashBlock = innerHash.getRawData();

    // o_key_pad || hash (i_key_pad || message)
    oKeyPad.append(innerHashBlock.getData(), innerHashBlock.getSize());

    // hash (o_key_pad || hash (i_key_pad || message))
    const SHA256 totalHash(oKeyPad);
    return totalHash.getRawData();
}

MemoryBlock hmacSha256(MemoryBlock key, String message)
{
    if (key.getSize() == 0 || message.isEmpty())
    {
        jassertfalse;
        return {};
    }

    MemoryBlock messageBlock;
    messageBlock.insert(message.toRawUTF8(), message.getNumBytesAsUTF8(), 0);

    return hmacSha256(key, messageBlock);
}

MemoryBlock hmacSha256(const String& key, const String& message)
{
    if (key.isEmpty() || message.isEmpty())
    {
        jassertfalse;
        return {};
    }

    MemoryBlock keyBlock, messageBlock;

    keyBlock.insert(key.toRawUTF8(), key.getNumBytesAsUTF8(), 0);

    messageBlock.insert(message.toRawUTF8(), message.getNumBytesAsUTF8(), 0);

    return hmacSha256(keyBlock, messageBlock);
}

void hmacTester()
{
    auto block = hmacSha256("1", "1");
    auto hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("6ff6ceedb1c100266849d7e4e13449c98d1c98e569994f8ddbfe7b52e780dead"));

    block = hmacSha256("2", "2");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("3457a8393e3d6a61a06d0c912567c35c81c3c93c393770586bda087bb66bf191"));

    // key & message order
    block = hmacSha256("2", "1");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("4703d481ddedca88184497744b52937586bef3b273645082c04529f73b85456e"));

    block = hmacSha256("1", "2");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("1094216ce4732c56802e8e40f3f0bbfb13c56cac406069ac6146d0532a1b0d4c"));

    // longer keys
    block = hmacSha256("123456789012345678901234567890123456789012345678901234567890123", "1");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("1b6820dc40123595adc38418b59421611b9a272e54f0bec582684ff243873f69"));

    block = hmacSha256("1234567890123456789012345678901234567890123456789012345678901234", "1");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("f24efc55ff17608d5245eb4a79fdce36fc8355ea4147bc61da3705d7303739dd"));

    block = hmacSha256("12345678901234567890123456789012345678901234567890123456789012345", "1");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("15a42817beae1ec488b6f78fedebe3cdfa50fd7f5e065c84b41299bf3856dd2f"));

    // onward feed
    block = hmacSha256("1", "1");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("6ff6ceedb1c100266849d7e4e13449c98d1c98e569994f8ddbfe7b52e780dead"));

    // calc using block
    auto block2 = hmacSha256(block, "1");
    const auto hexStr2 = String::toHexString(block2.getData(), int(block2.getSize()), 0);
    jassert(hexStr2.equalsIgnoreCase("c26bb3c2c019db16572c607de7f1b8afa9a5d00500949745670d0905994f8dd3"));

    // Note, the above is NOT the same as if we were to pass the previous hexstr in as a key
    block = hmacSha256("6ff6ceedb1c100266849d7e4e13449c98d1c98e569994f8ddbfe7b52e780dead", "1");
    hexStr = String::toHexString(block.getData(), int(block.getSize()), 0);
    jassert(hexStr.equalsIgnoreCase("705cdab4651f9dd010674c37131554c988596fed4f333be755023674c94068c4"));

}
