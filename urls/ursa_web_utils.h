/*
  ==============================================================================

    ursa_web_utils.h
    Created: 29 Mar 2019 9:20:20am
    Author:  davee

  ==============================================================================
*/

#pragma once

using namespace juce;


enum class HttpMethod
{
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH
};

class UrsaUrl;

struct UrlCallResults
{
    StringPairArray response_headers;
    int status_code{ 0 };
    int64 callDurationMillis{-1};
private:
    friend UrsaUrl;
    URL urlWithParams;
    std::unique_ptr<InputStream> input_stream;
};


/**
 * A class to prepare a multipart form for sending to the server using the appropriate UrsaUrl constructor
 * based on Juce's URL::createHeadersAndPostData 
 */
class UrsaUrlMultiPartForm
{
public:

    explicit UrsaUrlMultiPartForm ();

    /**
     * Add a file to this submission
     * The File will be read 
     */
    void addFile (const File& file, const String& parameterName, const String& fileMimeType, const String& fileName);
    void addFile (const File& file, const String& parameterName, const String& fileMimeType);

    void addParam (const String& key, const String& value);

    /**
     * Used to provide the full content header and the boundary
     */
    String getContentTypeHeader () const;

    /**
     * Creates a memory block containing the encoded form 
     */
    MemoryBlock encodeData () const ;

    /**
     * purely for reference only, allows you to see the boundary that will be used in this submission.
     */
    String getBoundary () const
    {
        return boundary;
    }

private:
    const String boundary;
    
    std::vector<std::pair<String,String>> textParams;

    struct FileParam
    {
        FileParam (const String& paramName, const String& filename, const String& mimeType, const File& file)
            : paramName (paramName),
              filename (filename),
              mimeType (mimeType),
              file (file)
        {
        }

        String paramName, filename, mimeType;
        File file;
    };

    std::vector<FileParam> fileParams;

};


/**
 * A class to wrap some of the oddities of the Juce URL
 */
class UrsaUrl
{
public:

    /**
     * \brief Use this when you want to send a block of memory - you MUST specify content type yourself
     * 
     * \param url the naked url with or without any parameters on the end
     * \param url_parameters additional parameters to add to the end of the URL (regardless of HTTP method) and NOT placed in the body
     * \param extra_headers headers to inject, note that this will not automatically add Content-Type, you need to do that because you know what is in the memory block
     * \param request_entity_body a memory block which will be streamed into the request as the entity body
     * \param method note that some choices, e.g. GET would be silly, but that's your prerogative.
     */
    explicit UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
        const MemoryBlock& request_entity_body, HttpMethod method);

    /**
     * \brief use this to send multipart/form-data to a url
     * 
     * params are as above except:
     * \param request_entity_body see the UrsaUrlMultiPartForm class 
     * \param method probably only makes sense to POST this, but you can override if you must.
     */
    explicit UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
        const UrsaUrlMultiPartForm& request_entity_body, HttpMethod method = HttpMethod::POST);

    /**
     *\brief Use this when you want to send a string as the request entity body 
     * otherwise as above
     * \param request_entity_body a string 
     */
    explicit UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
             const String& request_entity_body, HttpMethod method);

    /**
       *\brief Use this when you want to send a string as the request entity body
       * Otherwise as above, this exists to stop compiler ambiguity
       * 
       * \param request_entity_body a const char*
       */
    explicit UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
                      const char* request_entity_body, HttpMethod method);

    
    /**
        * \brief Use this when you want to send parameters that are encoded as per application/x-www-form-urlencoded
        * Params will be URL encoded and Content-Type: application/x-www-form-urlencoded will be automatically added to the extra_headers
        * \param body_params_to_encode treated as key value pairs, which will be URL encoded before sending
        * otherwise as above
        */
    explicit UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
        const StringPairArray& body_params_to_encode, HttpMethod method);

    
    
    explicit UrsaUrl (const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
             const nlohmann::json& request_entity_body_json, HttpMethod method);


    /**
     * Utility function when sending a file to the server, note that this implementation reads the whole file into memory first rather than streaming it.
     * you need to set the complete and correct content type header for the file e.g. "Content-Type: application/zip"
     * 
     * see: https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
     * see: https://developer.mozilla.org/en-US/docs/Web/HTTP/MIME_types/Common_types
     *
     */
    static UrsaUrl createFromFile(const String& url, const StringPairArray& url_parameters, const StringArray& extra_headers,
        const File& fileToSendAsBody, const String& contentTypeHeader, HttpMethod method = HttpMethod::PUT);


    ~UrsaUrl();

    /**
     * \brief treat the response as a JSON message and decode using nlohmann::json
     * \param logBlacklist if there are any json items that you want to supress from the logs, add their names here.
     */
    nlohmann::json get_json_response(StringArray logBlacklist = {}, bool logging = true) const;

    /**
     * Stop further logging of this call, including at destruction
     */
    void skipLogging()
    {
	    logWritten = true;
    }

    String getResponseContentType() const;

    String getStringResponse () const;

    /**
     * Call the URL.
     * By default anything below 200 or above 299 will cause the url to report this as an error
     * optional parameters allow this to be adjusted, for instance if your code can deal with a
     * 3xx code you would need to provide 200, 399 as the min and max.
     */
    int call (int minSuccessfulResponse = 200, int maxSuccessfulResponse = 299, int timeout = 15000);

	/**
	 * store a file response in a chunked way that responds to the thread needing to shut down.
	 */
    bool getAndStoreFileResponse (const File& saveFile) const;

    //bool getAndDecodeAudioResponse (AudioBuffer<float>& output) const;

    Image getImageResponse () const;

    /**
     * This causes the url response to be read and then written to a log.
     * Do not call this if you have already read the response as that will have written the log
     */
    void read_output_and_write_to_log () const;

    /**
     * reduce logging to remove request and response body text to stop this leaking credentials or tokens
     */
    void sensitiveLogging ();

    void logTimeMetrics ();

    static String http_method_to_string (HttpMethod method);

private:
    String baseURL;

    StringPairArray url_parameters;
    StringArray extra_headers;
    //String request_entity_body;
    MemoryBlock request_entity_body_block;
    HttpMethod method;

    int timeoutMs{ 15000 };

	std::unique_ptr<UrlCallResults> callResults;

    /// <summary>
    /// a variable used to check if this has written a log and either stop if from doing so again or make sure it does so at least once on destruct
    /// </summary>
    mutable bool logWritten{ false };

    // set this to true to stop bodies of requests and responses being included in the logs
    bool authenticationCall{ false };

	/**
	 * Looks for a header that starts with the given string and returns the rest of it.
	 */
	String findHeaderStarting(const String& start) const;


    String get_raw_string_response () const;

    /**
     * used to echo out some of the data from a call
     */
    //void innerWriteLogShortened(const String& responseText) const;

    /**
     * used to echo out ALL the data of a call
     * 
     */
    void innerWriteToLog (String entity_body_data, bool forceVerbose = false) const;

    //util method
    static String urlEncodeFormParams (const StringPairArray& stringPairs);
    
    static StringArray addContentTypeIfMissing(const StringArray& extraHeaders, String contentTypeHeader)
    {
        for(const auto& header : extraHeaders)
        {
            if(header.startsWithIgnoreCase ("Content-Type:"))
            {
                return extraHeaders;
            }
        }

        StringArray newArray = extraHeaders;
        newArray.add (contentTypeHeader);
        return newArray;
    }
    
};


void log_url_call (int status_code, URL url, const String request_entity_body, String response_body);

/**
 * \brief a sanitized way of calling a url for a number of HTTP methods
 * \param url the url to send to (without GET style URL params)
 * \param url_parameters the set of GET style URL params to add, these are still added to the URL for POST and other methods.
 * \param extra_headers additional headers to add to the HTTP Request e.g. "Content-Type: application/json\r\n"
 * \param request_entity_body the request data (i.e. the request body without the entity headers which go in 'extra_headers')
 * \param method the HTTP method to send this using.
 * \return a String containing the whole response entity body.
 */
String callUrlAndReturnText (URL url, StringPairArray url_parameters, StringArray extra_headers,
                             String request_entity_body, HttpMethod method);

/**
* \brief a sanitized way of calling a url for a number of HTTP methods
* \param url the url to send to (without GET style URL params)
* \param url_parameters the set of GET style URL params to add, these are still added to the URL for POST and other methods.
* \param extra_headers additional headers to add to the HTTP Request e.g. "Content-Type: application/json\r\n"
* \param request_entity_body the request data in JSON format
* \param method the HTTP method to send this using.
* \return a JSON containing the whole response entity body.
*/
nlohmann::json callJSONUrl (URL url, StringPairArray url_parameters, StringArray extra_headers,
                            nlohmann::json request_entity_body, HttpMethod method);


/**
 * Sha256 based HMAC function to create a 32byte digest of a message signed by a key.
 * Typically the key and message are strings, this method is used by the string based methods
 */
MemoryBlock hmacSha256(MemoryBlock key, MemoryBlock message);

/**
 * Sha256 based HMAC function to create a 32byte digest of a message signed by a key.
 * Typically the key and message are strings, this method allows the output of one HMAC to be passed as the key to another
 */
MemoryBlock hmacSha256(MemoryBlock key, String message);

/**
 * classic SHA256 based HMAC function. The message is signed using the
 *
 */
MemoryBlock hmacSha256(const String& key, const String& message);
